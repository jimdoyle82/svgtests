# SVG Tests

## Description
Experimenting with best approaches to using SVGs in an automated way. Intend of including custom Font Awesome characters, inline SVG injection and some kind of PhantomJS spriting. Only work out of "_dev" and only upload from "_dist/deploy/". Run GruntJS tasks to compile project.

## Running GruntJS tasks

### Installation
You'll need to install these command-line tools in this order:
- NodeJS (bundles with NPM): http://nodejs.org
- GruntJS (cli - requires NodeJS & NPM): `npm install -g grunt-cli`
- Ruby: (pre-installed on Mac, Windows: http://rubyinstaller.org)
- Sass 3.2.17 (not higher - requires Ruby): `gem install sass`
- Compass 0.12.2 (not higher - requires Ruby): `gem install compass`

### Root "Gruntfile.js"
Before you start building, you'll need to run `npm install`. This will download all the NodeJS dependencies you need, then will automatically run the `grunt update` task, which will update all the NodeJS dependencies within the "grunt_tasks" folder and then will run a `bower install` from the project root, which will install all your Bower dependencies and place them in "_dev/deps/bower_components".

The root "Gruntfile.js" does very little itself. It is responsible for:
- showing a message that outlines possible tasks to run `grunt`
- running the local dev server `grunt local`
- triggering other "Gruntfile.js" files within the "grunt_tasks" directory
- To run all tasks do `grunt build` or simply `grunt b`

### "grunt_tasks" directory
Putting all your logic into a single "Gruntfile.js" can be quite difficult to follow, especially for new developers coming into a project. So this proect separates the "js", "css" and "miscellaneous" tasks into different folders. Each of these can be run stand alone, from within their respective folders, or they can be triggered from the root "Gruntfile.js" using the "grunt-hub" task. Run `grunt` from the root "Gruntfile.js" to see possible options. 

### Debugging a task
If you are debugging an error with a particular "Gruntfile.js" file, you are best running the task from the same directory and using the `-v` flag - so `grunt -v` - because unfortunately "grunt-hub" doesn't show all debugging info. The `-v` or `--verbose` flag displays the extra debugging info.

### GruntJS website
For more info about GruntJS, visit their website http://www.gruntjs.com.


## Explanation of files in the project root

### README.md
Documentation of the overall project in markdown format. This will also generate a home page if you're using something like Github or BitBucket. Use tools like this to view view in a prettier format: http://www.markdownviewer.com/

### package.json
This a standard NodeJS required file that holds project info, such as version, author, repos and dependencies. It is required to run the "Gruntfile.js".

### Gruntfile.js
Main logic file for running build scripts.

### bower.json
Lists bower dependencies and is required in order to use Bower.

### .bowerrc
Optional bower file that specifies the location that bower should install into.

### .gitignore
Tells Git not to include certain files and folders, such as "node_modules", ".sass-cache", etc.

### ~grunt__local.bat
Shortcut for Windows users to run the local server just by double clicking it.


## Notes on inline SVGs
SVGs inside "_dev/app/svgdefs/" get processed into a single js "/_dist/deploy/js/svgloader.js", which dynamically embeds the SVG inline just after the body tag is opened. 

The following markup must be placed in the markup just after the body tag is opened.
```js
<span class="svgdefs"></span>
<script src="/_dist/deploy/js/svgloader.js"></script>
```

### Advantages of this approach to icons
All the benefits of Font Awesome, including a single header request for all icons, plus more:
- It's easier to update. Just add an SVG to the "_dev/app/svgdefs/" folder and do a build.
- Styles can be changed. By adding inline styles to markup directly, or via CSS, fills and strokes can be added.

### Drawbacks
- No support for IE8 and lower, or Android 2.3 and lower.

### About inline SVGs
Scroll down to 'Using "inline" SVG' - http://css-tricks.com/using-svg/

### About loading SVGs with js
http://www.pencilscoop.com/2014/04/injecting-svg-with-javascript/

### About inline SVGs with the <symbol> tag
http://css-tricks.com/svg-symbol-good-choice-icons/