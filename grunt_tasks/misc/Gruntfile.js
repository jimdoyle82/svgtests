
module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON("../../package.json")

        ,vars: {
            root:           "../../"
            ,dev:           "<%= vars.root %>_dev/"
            ,dist:          "<%= vars.root %>_dist/"
            ,deploy:        "<%= vars.dist %>deploy/"
            ,app:           "<%= vars.dev %>app/"
            ,devmarkup:     "<%= vars.app %>markup/"
            ,deps:          "<%= vars.dev %>deps/"
            ,bowcomp:       "<%= vars.deps %>bower_components/"
            ,versioned:     "<%= vars.deps %>libs/versioned/"
            ,dist_img_singles: "<%= vars.dist %>tmp/singles-uncompressed/"
            ,banner_content:    '<%= pkg.name %> - <%= pkg.version %> - ' + grunt.template.today("yyyy-mm-dd")
        }


        ,copy: {
            // we don't need to deploy these because thay come from CDN, so they just get copied to dist
            fonts: {
                expand: true
                ,flatten: false
                ,cwd: "<%= vars.dev %>app/fonts/"
                ,src: [  "**/*" ]
                ,dest: "<%= vars.dist %>fonts/" 
            }
            
            // copies all non-sprite images
            ,appimg: {
                 expand: true
                ,flatten: true
                ,cwd: "<%= vars.dev %>"
                ,src: [  "app/img/singles/*.{png,gif,jpg}" ]
                ,dest: "<%= vars.dist_img_singles %>"
            }
        }



        ,imagemin: {

            // sprites already get compressed in "sass" task

            singles: {
                options: {
                    pngquant: true
                    ,optimizationLevel: 3
                }
                ,files: [{
                     expand: true
                    ,flatten: true
                    ,cwd: '<%= vars.dist_img_singles %>'
                    ,src: ['*.{png,jpg,gif}']
                    ,dest: '<%= vars.deploy %>img/'
                }]
            }
        }


        ,htmlmin: {
            dist: {
                options: {
                    removeComments: true
                    ,collapseWhitespace: true
                }
                ,files: {
                    "<%= vars.deploy %>index.html": "<%= vars.deploy %>index.html"
                }
            }
        }

        ,cacheBust: {
            options: {
              rename: true // do rename files
              ,dir: "<%= vars.deploy %>" // base url to look for assets
              ,ignorePatterns: ["libs"] // libs shouldn't be changing, so ignore them
            },
            assets: {
                files: [{
                    src: ['<%= vars.deploy %>index.html']
                }]
            }
        }

    });
        

    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-cache-bust');

    grunt.registerTask("default", [ "copy"
                                    ,"imagemin"
                                    ,"dist-markup-head"
                                    ,"dist-markup-populate"
                                    ,"htmlmin"
                                    ,"cacheBust"
                                    ,"set-absolute-paths"
                                ]);


    grunt.registerTask('dist-markup-head'
        ,"Copies content in dev index.html head (within custom tags) into 'dist/deploy/index-uncompressed.html'."
        ,function() {
        var distHtml = grunt.file.read( grunt.config("vars.deploy") + "index-uncompressed.html" )
            ,devHtml = grunt.file.read( grunt.config("vars.app") + "index.html" );

        var tagStart = "<!--{GRUNT_HEAD_MARKUP_START}-->"
            tagEnd = "<!--{GRUNT_HEAD_MARKUP_END}-->";

        var devContent = devHtml.split(tagStart)[1].split(tagEnd)[0];
        var distAll = distHtml.split(tagStart)[0] + tagStart + devContent + tagEnd + distHtml.split(tagEnd)[1];

        grunt.file.write( grunt.config("vars.deploy") + "index.html", distAll );
    });


    grunt.registerTask('dist-markup-populate', 
        'fills "dist/deploy/index.html" with markup ready for production'
        ,function() {
        
        var distHtml    = grunt.file.read( grunt.config("vars.deploy")    + "index.html" )
            ,bodyHtml   = grunt.file.read( grunt.config("vars.devmarkup") + "body.html" );

        var distAll = distHtml.replace( "<!--{{GRUNT_PLACE_BODY}}-->", bodyHtml );

        grunt.file.write( grunt.config("vars.deploy") + "index.html", distAll );
    });



    grunt.registerTask("set-absolute-paths"
        ,"Because the 'cacheBust' task needs relative paths, we have to run this task at the end to make paths absolute."
        ,function() {

        var html = grunt.file.read( grunt.config("vars.deploy") + "index.html" );
        
        html = html.split("css/debug/").join("/_dist/deploy/css/min/");
        html = html.split("js/app.min").join("/_dist/deploy/js/app.min");
        html = html.split('src="libs/').join('src="/_dist/deploy/libs/');

        grunt.file.write( grunt.config("vars.deploy") + "index.html", html );
    });

};
