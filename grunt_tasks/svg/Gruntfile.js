
module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON("../../package.json")

        ,vars: {
            root:           "../../"
            ,dev:           "<%= vars.root %>_dev/"
            ,dist:          "<%= vars.root %>_dist/"
            ,deploy:        "<%= vars.dist %>deploy/"
            ,app:           "<%= vars.dev %>app/"
            ,devmarkup:     "<%= vars.app %>markup/"
            ,deps:          "<%= vars.dev %>deps/"
            ,bowcomp:       "<%= vars.deps %>bower_components/"
            ,versioned:     "<%= vars.deps %>libs/versioned/"
            ,dist_img_singles: "<%= vars.dist %>tmp/singles-uncompressed/"
            ,banner_content:    '<%= pkg.name %> - <%= pkg.version %> - ' + grunt.template.today("yyyy-mm-dd")
        }



        // optimises SVGs by stripping out Illustrator export bloat
        ,svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= vars.app %>svgdefs',
                    src: ['*.svg'],
                    dest: '<%= vars.dist %>tmp/svgmin/',
                    ext: '.svg'
                }]
            }
        }


        ,svgstore: {
            dist: {
                options: {
                    prefix : 'dm-'
                }
                ,files: {
                    "<%= vars.dist %>tmp/svgdefs.min.svg": "<%= vars.dist %>tmp/svgmin/*.svg"
                }
            }
        }


        ,webfont: {
            icons: {
                src: "<%= vars.dist %>tmp/svgmin/*.svg"
                ,dest: "<%= vars.deploy %>fonts/"
                ,options: {
                    font: "dmicon"
                    ,hashes: false // don't want unique urls
                    // ,stylesheet: "scss"
                    // ,htmlDemo: false
                    ,relativeFontPath: "/_dist/deploy/fonts/"
                    ,engine: "node"
                    ,templateOptions: {
                        baseClass: 'dmicon'
                        ,classPrefix: 'dmicon_'
                        // ,mixinPrefix: 'glyph-'
                    }
                }
            }
        }

        ,grunticon: {
            deploy: {
                files: [{
                    expand: true,
                    cwd: '<%= vars.dist %>tmp/grunticon-prep/',
                    src: ['*.svg'],//, '*.png'
                    dest: "<%= vars.deploy %>img/iconsprites-tmp/"
                }],
                options: {
                    pngfolder: "../iconsprites/"
                    ,pngpath: "/_dist/deploy/img/iconsprites/"
                    ,defaultWidth: "100px"
                    ,defaultHeight: "100px"
                    ,template: "./grunticon.hbs"
                }
            }
        }

        ,copy: {
            grunticonPrep: {
                expand: true
                ,flatten: false
                ,cwd: "<%= vars.app %>svgdefs/"
                ,src: [  "*.svg" ]
                ,dest: "<%= vars.dist %>tmp/grunticon-prep/"
            }
            ,grunticonPost: {
                src: "<%= vars.deploy %>img/iconsprites-tmp/icons.fallback.css"
                ,dest: "<%= vars.deploy %>img/iconsprites/icons.fallback.css"
            }
        }

        ,clean: {
            options: { force: true }
            ,grunticon: [   "<%= vars.dist %>tmp/grunticon-prep/"
                            ,"<%= vars.deploy %>img/iconsprites-tmp/"
                        ]
        }

    });
        

    grunt.loadNpmTasks('grunt-svgmin');
    grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-webfont');
    grunt.loadNpmTasks('grunt-grunticon');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask("default", [ "svgmin"
                                    ,"webfont"
                                    ,"svgstore"
                                    ,"insert-svgdefs"
                                    ,"pngfallback"
                                ]);


    grunt.registerTask("insert-svgdefs", function() {

        var defs = grunt.file.read( grunt.config.get("vars.dist") + "tmp/svgdefs.min.svg" );

        var js =    "document.addEventListener('DOMContentLoaded', function() {"+
                        "var i=0,svgs = document.querySelectorAll('.svgdefs');" +
                        "for (;i<svgs.length; i++) { svgs[i].innerHTML = '"+defs+"'};"+
                    "});";

        grunt.file.write( grunt.config.get("vars.deploy") + "js/svgloader.js", js );

    });

    grunt.registerTask("pngfallback", ["copy:grunticonPrep", "grunticon-prep", "grunticon", "copy:grunticonPost", "clean:grunticon"]);


    // Adds some extra markup that could be missing from SVGs in order to make grunticon compatible
    grunt.registerTask("grunticon-prep", "Prep svgs for grunticon", function() {

        grunt.file.recurse( grunt.config("vars.dist") + "tmp/grunticon-prep/", function(abspath, rootdir, subdir, filename) {

            var contents = grunt.file.read( abspath );

            if( contents.indexOf("<?xml") === -1 )
                contents = '<?xml version="1.0" encoding="utf-8"?>\n' + contents;

            contents = contents.split("<symbol").join("<g").split("</symbol").join("</g");
            contents = contents.split("<svg ").join('<svg xmlns="http://www.w3.org/2000/svg" ');

            grunt.file.write( abspath, contents );
        });

    });

};
