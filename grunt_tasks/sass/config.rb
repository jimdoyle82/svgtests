
require 'animate-sass'

# Required config if using sprites

# this is where the generated sprite will get saved
images_dir = "../../_dist/img/sprites-uncompressed/"

# this is the absolute path that the sprites will be for production
http_generated_images_path = "/_dist/deploy/img/sprites/"

# this is where your original images will get fetched from
images_path = "../../_dist/img/"