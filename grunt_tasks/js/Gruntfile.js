module.exports = function( grunt ) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('../../package.json')

		,vars: {
            root:           "../../"
            ,dev:           "<%= vars.root %>_dev/"
            ,dist:          "<%= vars.root %>_dist/"
            ,deploy:        "<%= vars.dist %>deploy/"
            ,app:           "<%= vars.dev %>app/"
            ,deps:          "<%= vars.dev %>deps/"
            ,bowcomp:       "<%= vars.deps %>bower_components/"
            ,dist_img_singles: "<%= vars.dist %>img/singles-uncompressed/"
            ,banner_content:    '<%= pkg.name %> - <%= pkg.version %> - ' + grunt.template.today("yyyy-mm-dd")
        }

        ,clean: {
            options: { force: true }
            ,libs:      [ "<%= vars.deploy %>libs/*" ]
        }

        ,copy: {
            // copies deps into _dist/deploy that DO already have minified versions
            libs: {
                expand: true
                ,flatten: true
                ,cwd: "<%= vars.deps %>"
                ,src: [ "libs/jquery-1.8.0.min.js"
                    ]
                ,dest: "<%= vars.deploy %>libs/"
            }
        }

        ,concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: [  
                        '<%= vars.deps %>libs/modernizr.custom.26580.js'
                        ,'<%= vars.app %>js/App.js'
                    ]
                ,dest: '<%= vars.deploy %>js/app.debug.js'
            }
        }

        ,removelogging: {
            dist: {
                options: {
                    replaceWith: 0 // replaces with 0 so that "if" statements have something to compare
                    ,methods: [ "log" ] // keeps all console statements except for "log"
                }
                ,src: '<%= vars.deploy %>js/app.debug.js'
                ,dest: '<%= vars.deploy %>js/app.debug.js'
            }
        }

        ,uglify: {
            options: {
                 preserveComments: "some"
                ,banner: '/** <%= vars.banner_content %> */\n'
                ,compress: {
                    global_defs: { "DEBUG": false }
                }
            },
            libs: {
                // minify deps that DON'T currently have minified versions
                files: [
                        {
                            src: '<%= vars.deploy %>js/app.debug.js'
                            ,dest: '<%= vars.deploy %>js/app.min.js'                            
                        }
                    ]
            }
        }
	});

	grunt.registerTask("default", ["clean", "copy", "concat", "removelogging", "uglify"]);

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-remove-logging');
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
}