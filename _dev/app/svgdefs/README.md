# SVG definitions
These SVGs get processed into a single js "/_dist/deploy/js/svgloader.js", which dynamically embeds the SVG inline just after the body tag is opened. Please see project root "README.md" for more info on this, under "Notes on SVGs".

## Font Awesome
- Requires the `viewBox` attribute on the `<svg>` tag.

## Inline SVGs
- Use `<symbol>` tag to include screen reader friendly `<title>` and `<desc>` tags. 
- You should also add the `viewBox` attibute to the `<symbol>`, so that you don't need to define it on the svg instance tag.
- `id` attibute will automatically get added and will take the file name of the source svg.

## PNG fallback
- You have to specify the width and height in the svg for the PNG fallback to work. There may be a better way of doing this, but not sure yet.
- Currently no flexible way to specify different sizes for PNGs. Just a single size in the SVG. This could be done in the build though, possibly by copying some comment data to produce various sizes.
- Colourising options are possible with Grunt Icon, but I have explored them yet.

## Illustrator Notes
- Adobe Illustrator won't recognise the `<symbol>` tag, so change it to a `<g>` tag if you need to edit it, then change it back.
- It will also remove any embedded comments too!
- It adds crap to the top of the svg that you don't need.